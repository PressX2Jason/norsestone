#pragma once
#include "SplashScreen.h"
#include "MainMenu.h"
#include "iostream"

class Game
{
public:
	static void start();

	static const int WINDOW_WIDTH = 1024;
	static const int WINDOW_HEIGHT = 768;
	static const std::string WINDOW_TITLE;
	static const std::string VERSION;

private:
	static bool isExiting();
	static void gameLoop();

	static void showSplash();
	static void showMenu();

	enum GameState { UNINITIALIZED, SHOWING_SPLASH, PAUSED, SHOWING_MENU, PLAYING, EDIT, EXITING };
	static GameState gameState;
	static sf::RenderWindow mainWindow;
};

