#pragma once
#include <list>


class MainMenu
{
public:
	enum MenuResult { NOTHING, PLAY, EDIT_DECK, EXIT};

	struct MenuItem{
		sf::IntRect rect;
		MenuResult action;
	};

	MenuResult	show(sf::RenderWindow& window);

private:
	MenuResult getMenuResponse(sf::RenderWindow& window);
	MenuResult handlleMouseClick(int x, int y);

	MenuItem setupPlayButton();
	MenuItem setupEditDeckButton();
	MenuItem setupExitButton();

	std::list<MenuItem> menuItemList;
};
