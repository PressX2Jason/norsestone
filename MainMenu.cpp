#include "stdafx.h"
#include "MainMenu.h"
#include "iostream"

MainMenu::MenuResult MainMenu::show(sf::RenderWindow& window){
	//load image from file
	sf::Texture texture;
	if (texture.loadFromFile("images/tempMenu.png") != true)
	{
		std::cout << "Error: MainMenu not loaded" << std::endl;
		return EXIT;
	}
	sf::Sprite sprite(texture);

	//setup clickable regions
	menuItemList.push_back(setupPlayButton());
	menuItemList.push_back(setupEditDeckButton());
	menuItemList.push_back(setupExitButton());

	//draw the menu
	window.draw(sprite);
	window.display();

	return getMenuResponse(window);
}

MainMenu::MenuResult MainMenu::getMenuResponse(sf::RenderWindow& window){
	//main event loop for the window
	sf::Event event;

	while (window.pollEvent(event)){
		if (event.type == sf::Event::EventType::MouseButtonPressed){
			return handlleMouseClick(event.mouseButton.x, event.mouseButton.y);
		}

		if (event.type == sf::Event::EventType::Closed){
			return EXIT;
		}
	}

}

MainMenu::MenuResult MainMenu::handlleMouseClick(int x, int y){
	std::list<MenuItem>::iterator it = menuItemList.begin();

	while (it != menuItemList.end()){
		sf::IntRect menuItemRect = it->rect;

		if (menuItemRect.contains(x, y)){
			std::cout << it->action << std::endl;
			return it->action;
		}
		it++;
	}
}


MainMenu::MenuItem MainMenu::setupPlayButton(){
	MenuItem menuItem;
	menuItem.rect.left = 242;
	menuItem.rect.top = 103;

	menuItem.rect.height = 150;
	menuItem.rect.width = 780;
	menuItem.action = PLAY;
	return menuItem;
}

MainMenu::MenuItem MainMenu::setupEditDeckButton(){
	MenuItem menuItem;
	menuItem.rect.left = 242;
	menuItem.rect.top = 225;

	menuItem.rect.height = 155;
	menuItem.rect.width = 780;
	menuItem.action = EDIT_DECK;
	return menuItem;
}

MainMenu::MenuItem MainMenu::setupExitButton(){
	MenuItem menuItem;
	menuItem.rect.left = 242;
	menuItem.rect.top = 390;

	menuItem.rect.height = 155;
	menuItem.rect.width = 780;
	menuItem.action = EXIT;
	return menuItem;
}