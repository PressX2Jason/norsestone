#include "stdafx.h"
#include "Game.h"

//initialize static  members
const std::string Game::WINDOW_TITLE = "NorseStone";
//Major.Minor.Release.Build Number
const std::string Game::VERSION = "0.0.0.1";

Game::GameState Game::gameState;
sf::RenderWindow Game::mainWindow;


void Game::start(void){
	if (gameState != UNINITIALIZED)
		return;

	mainWindow.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, 32), WINDOW_TITLE + " " + VERSION);
	gameState = SHOWING_SPLASH;

	while (!isExiting()){
		gameLoop();
	}

	mainWindow.close();

}

bool Game::isExiting(){
	return gameState == EXITING;
}

void Game::gameLoop(){
	sf::Event event;

	while (mainWindow.pollEvent(event)){
		switch (gameState){
		case PLAYING:{
			mainWindow.clear(sf::Color(0.0f, 0.0f, 0.0f));
			mainWindow.display();
			if (event.type == sf::Event::EventType::Closed){
				gameState = EXITING;
			}
			break;
		}
		case SHOWING_MENU:{
			showMenu();
			break;
		}
		case SHOWING_SPLASH:{
			showSplash();
			break;
		}

		}
	}
}

void Game::showSplash(){
	SplashScreen splashScreen;
	splashScreen.show(mainWindow);
	gameState = SHOWING_MENU;
}

void Game::showMenu(){
	MainMenu mainMenu;
	MainMenu::MenuResult menuResult = mainMenu.show(mainWindow);

	switch (menuResult){
	case MainMenu::PLAY:{
		std::cout << "Play selected" << std::endl;
		gameState = PLAYING;
		break;
	}
	case MainMenu::EDIT_DECK:{
		std::cout << "Edit selected" << std::endl;
		gameState = EDIT;
		break;
	}
	case MainMenu::EXIT:{
		std::cout << "Exit selected" << std::endl;
		gameState = EXITING;
		break;
	}
	}
}
